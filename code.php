<?php



function getFullAddress($specific, $city, $province, $country){
	return "$specific, $city, $province, $country";
}



function getLetterGrade($score){
	if($score < 75){
		return "$score is equivalent to F";
	}else if($score == 75 || $score == 76){
		return "$score is equivalent to C-";
	}else if($score >=75 && $score <= 79){
		return "$score is equivalent to C";
	}else if($score >=80 && $score <= 82){
		return "$score is equivalent to C+";
	}else if($score >=83 && $score <= 85){
		return "$score is equivalent to B-";
	}else if($score >=86 && $score <= 88){
		return "$score is equivalent to B";
	}else if($score >=89 && $score <= 91){
		return "$score is equivalent to B+";
	}else if($score >=92 && $score <= 94){
		return "$score is equivalent to A-";
	}else if($score >=95 && $score <= 97){
		return "$score is equivalent to A";
	}else if($score >=98 && $score <= 100){
		return "$score is equivalent to A+";
	}else {
		return "invalid score";
	}
};