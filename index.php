<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S1 Activity</title>
	</head>
	<body>
		<h1>Full  Address</h1>
		<p><?= getFullAddress("3F Caswyn Bldg., Timog Avenue", "Quezon City", "Metro Manila", "Philippines") ?></p>

		<p><?= getFullAddress("3F Enzo Bldg., Buendia Avenue", "Makati City", "Metro Manila", "Philippines") ?></p>

		<h1>Letter-Based Grading</h1>
		<p><?= getLetterGrade(87) ?></p>
		<p><?= getLetterGrade(94) ?></p>
		<p><?= getLetterGrade(74) ?></p>
	</body>
</html>